//
//  Datalist.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//

import RealmSwift
import Foundation

class Box: Object {
    // 管理用 ID。プライマリーキー
    @objc dynamic var id = 0

    // タイトル
    @objc dynamic var title = ""

    // 内容
    @objc dynamic var imagelinks = ""

    // 日時
    @objc dynamic var date = Date()

    // id をプライマリーキーとして設定
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
    
}

class Eiga: Object{
    
    // 管理用 ID。プライマリーキー
     @objc dynamic var eiga_id = 0
    
     @objc dynamic var connect_id = 0

     // タイトル
     @objc dynamic var eiga_title = ""

     // 内容
     @objc dynamic var eiga_contents = ""
    
     @objc dynamic var eigaimagelinks = ""
    
    // let boxs = LinkingObjects(fromType: Box.self, property: "eigas")
     // id をプライマリーキーとして設定
     override static func primaryKey() -> String? {
         return "eiga_id"
     }
}
