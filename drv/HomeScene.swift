//
//  level1BoxMovie.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//

import SwiftUI

import RealmSwift

struct HomeScene: View {
    var body: some View {
        ZStack {
            // 背景色の設定ss
            Color(.black)
                // セーフエリアを越えて全画面に表示する設定
                .edgesIgnoringSafeArea(.all)
                    
            ScrollView {
                VStack {
                    // アイコンの設定
                    HStack {
                        // アイコン画像の設定
                        Image(systemName: "house.fill")
                        .foregroundColor(.orange)
                         // ユーザー名の設定
                        Text("name")
                            .foregroundColor(Color.white)
                     
                    }
                    .padding(.top, 30.0)
                    .padding(.leading, -165.0)
                    
                    // 1つ目のボックスの設定
                    VStack {
                        // ボックス画像の設定
                        Image("BoxImage1")
                            .resizable()
                            // ボックス画像のサイズ
                            .frame(width: 270, height: 160)
                            // ボックスの影
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                        // ボックスタイトルの設定
                        Text("休日に観たい のんびり映画3選")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .padding(.init(top: 7,
                            leading: 10,
                            bottom: 1,
                            trailing: 10))
                            .foregroundColor(Color.white)
                    }
                    .padding(.horizontal, 37)
                    .padding(.vertical, 17)
                        // ボックスの背景色を設定
                    .background(Color("BoxColor1"))
                    .cornerRadius(12)
                }
                // 2つ目のボックスの設定
                VStack {
                    // アイコンの設定
                    HStack {
                        // アイコン画像の設定
                        Image(systemName: "house.fill")
                        .foregroundColor(.orange)
                        // ユーザー名の設定
                        Text("name")
                            .foregroundColor(Color.white)
                        Spacer()
                    }
                    .padding(.top, 10.0)
                    .padding(.leading, 45.0)
                    
                    VStack {
                        // ボックス画像の設定
                        Image("BoxImage2")
                            .resizable()
                            // ボックス画像のサイズ
                            .frame(width: 270, height: 160)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                        // ボックスタイトルの設定
                        Text("私のハワイアン映画リスト")
                            .font(.headline)
                                 
                            .fontWeight(.bold)
                            .padding(.init(top: 7,
                                           leading: 10,
                                           bottom: 1,
                                           trailing: 10))
                            .foregroundColor(Color.white)
                    }
                    .padding(.horizontal, 37)
                    .padding(.vertical, 17)
                    .background(Color("BoxColor2"))
                    .cornerRadius(12)
                }
                // 3つ目のボックスの設定
                VStack {
                    // アイコンの設定
                    HStack {
                        // アイコン画像の設定
                        Image(systemName: "house.fill")
                        .foregroundColor(.orange)
                        Text("name")
                            .foregroundColor(Color.white)
                        Spacer()
                    }
                    .padding(.top, 10.0)
                    .padding(.leading, 45.0)
                    VStack {
                        // ボックス画像の設定
                        Image("BoxImage3")
                            .resizable()
                            // ボックス画像のサイズ
                            .frame(width: 270, height: 160)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                        // ボックスタイトルの設定
                        Text("夜更かし専用の映画リスト🌙")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .padding(.init(top: 7,
                            leading: 10,
                            bottom: 1,
                            trailing: 10))
                            .foregroundColor(Color.white)
                    }
                    .padding(.horizontal, 37)
                    .padding(.vertical, 17)
                    .background(Color("BoxColor3"))
                    .cornerRadius(12)
                }
            }
        }
    }
}

struct HomeScene_Previews: PreviewProvider {
    static var previews: some View {
        HomeScene()
    }
}
