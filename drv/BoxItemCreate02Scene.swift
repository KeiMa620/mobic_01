//
//  InputEiga.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftUI

struct BoxItemCreate02Scene: View {
    @State public var itemTitle2 = ""
    @State public var contentbox2 = ""
    @State var isActiveSubView = false
    @State var image: UIImage?
    @State public var inputSe:Int = 1
    @EnvironmentObject var datastore: DataStore
    
    public var realm = try! Realm()
    var eiga: Eiga!

    var body: some View {
        NavigationView {
                ZStack {
                Color(self.datastore.boxColor)
                    .edgesIgnoringSafeArea(.all)
           
                VStack {
                    Spacer()
                    HStack {
                        TextField("", text: $itemTitle2)
                            .padding(.vertical, 7.0)
                            .foregroundColor(Color.white)
                            .multilineTextAlignment(.center)
                            .onAppear {
                                self.itemTitle2 = "default text"
                            }
                    }
                                   
                    .background(Color("grayblack"))
                    Spacer()
                    Spacer()
                    ImagePickAndDisplaySwiftUIView2(inputSe:self.$inputSe)
                    Spacer()
                    HStack {
                        Spacer()
                                    
                        TextField("", text: $contentbox2)
                            .lineLimit(5)
                            .foregroundColor(Color.white)
                            .frame(width: 350, height: 160)
                            .background(Color("sukedark"))
                            .cornerRadius(12)
                        Spacer()
                    }
                                    
                   Spacer()
                }
            }
        }
    }
}

struct BoxItemCreate02Scene_Previews: PreviewProvider {
    static var previews: some View {
        BoxItemCreate02Scene().environmentObject(DataStore())
    }
}
