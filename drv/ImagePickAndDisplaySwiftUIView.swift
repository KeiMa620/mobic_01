//
//  ImagePickAndDisplaySwiftUIView.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//

import SwiftUI
import Combine
import UIKit

struct ImagePickAndDisplaySwiftUIView: View {
    @State var image: UIImage?
    @State var showImagePicker: Bool = false
    
    var body: some View {
        VStack(alignment: .center, spacing: 16) {
            if image == nil {
                Rectangle()
                    .frame(width: 310, height: 200, alignment: .top)
            } else {
                Image(uiImage: image!)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 310, height: 200, alignment: .top)
                    .clipped()
            }
            Button(action: {
                self.showImagePicker = true
            }) {
                Text("Image Select")
            }
        }.sheet(isPresented: $showImagePicker) {
            BoxFaceImage_Scene(image: self.$image)
        }
    }
}

struct ImagePickAndDisplaySwiftUIView_Preview: PreviewProvider {
    static var previews: some View {
        return ImagePickAndDisplaySwiftUIView()
    }
}
