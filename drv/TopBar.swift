//
//  TopBar.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//

import SwiftUI

struct TopBar: View {
    var body: some View {
        ZStack(alignment: .top) {
            NavigationView {
            Text("Hello, World!")
              .navigationBarTitle("", displayMode: .inline)
          }
          Image("mobic_logo")
            .resizable()
            .scaledToFit()
            .frame(width: nil, height: 44, alignment: .center)
        }
      }
     }

struct TopBar_Previews: PreviewProvider {
    static var previews: some View {
        TopBar()
    }
}
