//
//  level1DataMovie.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//

import RealmSwift
import SwiftUI

struct BoxDetailScene01: View {
    var body: some View {
        ZStack {
            // 背景色を設定a
            Color("BoxColor1")
                .edgesIgnoringSafeArea(.all)
            
            ScrollView {
                VStack(spacing: 3) {
                    VStack(spacing: 40) {
                        // ボックスタイトルの設定
                        Text("休日に観たい映画3選")
                            .font(Font.custom("KohinoorDevanagari-Semibold", size: 25.0))
                                                       
                            // .fontWeight(.bold)
                            .padding(.init(top: 55,
                                           leading: 10,
                                           bottom: 1,
                                           trailing: 10))
                            .foregroundColor(Color.white)
                            .multilineTextAlignment(.center)
                            .frame(maxWidth: .infinity, maxHeight: 30)
                         // ボックス画像の設定
                        Image("BoxImage1")
                            .resizable()
                            .frame(width: 360, height: 220)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                            .padding(.bottom, 35)
                            .padding(.leading)
                            .padding(.trailing)
                    }
                    
                    // 1つ目のアイテムを設定
                    HStack {
                        Spacer()
                    
                        VStack {
                            Spacer()
                       
                            HStack {
                                Text("始まりのうた")
                                    .foregroundColor(Color.white)
                                    .multilineTextAlignment(.leading)
                                    .padding(.leading, 15.0)
                            
                                Spacer()
                            }.background(Color("grayblack"))
                        
                            HStack(alignment: .top) {
                                Spacer()
                                 // アイテム画像の設定
                                Image("ItemImage1")
                                    .resizable()
                                    .frame(width: 70, height: 100)
                            
                                Text("第80回アカデミー賞歌曲賞を受賞したONECEダンブリングのジョン監督が音楽をテーマにしたヒューマン映画")
                                    .lineLimit(10)
                                    .foregroundColor(Color.white)
                                Spacer()
                            }
                            Spacer()
                        }
                        .background(Color("sukedark"))
                        .frame(width: 350, height: 180)
                        .cornerRadius(10)
                        .padding(.leading)
                        .padding(.trailing)
                        Spacer()
                    }
                    .padding(.bottom, 10.0)
                    // 2つ目のアイテムを設定
                    HStack {
                        Spacer()
                               
                        VStack {
                            Spacer()
                            HStack {
                                // アイテム名の設定
                                Text("アメリ")
                                    .foregroundColor(Color.white)
                                    .multilineTextAlignment(.leading)
                                    .padding(.leading, 15.0)
                                                      
                                Spacer()
                            }.background(Color("grayblack"))
                                   
                            HStack(alignment: .top) {
                                Spacer()
                                // アイテム画像の設定
                                Image("ItemImage2")
                                    .resizable()
                                    .frame(width: 70, height: 100)
                                //アイテムの説明
                                Text("2001年4月に公開されたフランス映画．パリ・モンマルトルを舞台に、パリジャンの日常を描き、フランスで国民的大ヒットを記録した")
                                    .lineLimit(10)
                                    .foregroundColor(Color.white)
                                Spacer()
                            }
                            Spacer()
                        }
                        .background(Color("sukedark"))
                        .frame(width: 360, height: 180)
                        .cornerRadius(10)
                        .padding(.leading)
                        .padding(.trailing)
                        Spacer()
                    }
                }
                .padding(.bottom, 10.0)
                // 3つ目のアイテムを設定
        
                HStack {
                    Spacer()
                               
                    VStack {
                        Spacer()
                        HStack {
                            // アイテム名の設定
                            Text("LEON")
                                .foregroundColor(Color.white)
                                .multilineTextAlignment(.leading)
                                .padding(.leading, 15.0)
                                                      
                            Spacer()
                        }.background(Color("grayblack"))
                                   
                        HStack(alignment: .top) {
                            Spacer()
                            // アイテム画像の設定
                            Image("ItemImage3")
                                .resizable()
                                .frame(width: 70, height: 100)
                            // アイテム説明
                            Text("無骨な殺し屋を演じたジャン・レノには、その儚くも幼気な純真さに世の大半の女性は愛おしさを感じるだろう")
                                .lineLimit(10)
                                .foregroundColor(Color.white)
                            Spacer()
                        }
                        Spacer()
                    }
                    .background(Color("sukedark"))
                    .frame(width: 360, height: 180)
                    .cornerRadius(10)
                    .padding(.bottom, 35)
                    .padding(.leading)
                    .padding(.trailing)
                    Spacer()
                }
                Spacer()
            }
        }
    }
}

struct BoxDetailScene01_Previews: PreviewProvider {
    static var previews: some View {
        BoxDetailScene01()
    }
}
