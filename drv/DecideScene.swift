//
//  DecideScene.swift
//  drv
//
//  Created by け on 2020/08/22.
//  Copyright © 2020 ke. All rights reserved.
//

import SwiftUI
import Firebase
import FirebaseDatabase

struct DecideScene: View {
    @EnvironmentObject var datastore: DataStore
    let db = Firestore.firestore()
    let data = Data()
    var ref = Database.database().reference()
    var body: some View {
        ZStack {
        Color(self.datastore.boxColor)
            .edgesIgnoringSafeArea(.all)
        
       
        VStack {
            
                VStack {
                    Text("休日に観たい のんびり映画3選")
                        .font(.body)
                        .fontWeight(.bold)
                        .foregroundColor(Color.white)
                        .multilineTextAlignment(.center)
        
                    Image("BoxImage1")
                        .resizable()
                        .frame(width: 280, height: 180)
                        .shadow(color: .black, radius: 1, x: 2, y: 2)
                    
                    Text("Boxを投稿しますか？")
                    .font(.subheadline)
                    .fontWeight(.bold)
                    .foregroundColor(Color.white)
                        .multilineTextAlignment(.center)
                        .padding(.vertical, 15.0)
                    Button(action: {
                        self.save()
                        self.saveEiga()
                    }) {
                        Text("YES")
                        .frame(width: 180, height: 40)
                        .background(Color.white)
                        .cornerRadius(8)
                        .foregroundColor(.black)
                    }
                    .padding(.bottom, 6.0)
                    
                    Button(action: {}) {
                        Text("NO")
                        .frame(width: 180, height: 40)
                        .background(Color.white)
                        .cornerRadius(8)
                        .foregroundColor(.black)
                    }
                    
                }
                 .padding(.horizontal, 35)
                 .padding(.vertical, 40)
                 .background(Color("grayblack"))
                 .cornerRadius(12)
                }
            
            }
        
        
}

    private func save(){
        var medicines = [Int]()
        var medic:Int = 0
        db.collection("data").order(by: "BoxNum").limit(to: 1).getDocuments(){ (querySnapshot, err) in
          if let err = err {
              print("Error getting documents: \(err)")
          } else {
           for document in querySnapshot!.documents {
            let medicineToBeAdded = document.get("BoxNum") as! Int
               medicines.append(medicineToBeAdded)
            medic = medicines[0] + 1
            print("\(medicines)s\(medic)")
           }
                
              }
        }

        let docData: [String: Any] = [
            "BoxNum":medic,
            "BoxTitle": self.datastore.boxTitle,
            "BoxFaceImage": self.datastore.boxFaceImage,
            "BoxColor": self.datastore.boxColor,
            "Data":data
            ]
        db.collection("data").document().setData(docData) { err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("Document successfully written!")
            }
        }
    }
    
    private func saveEiga(){
        for i in 0..<2{
            
        let eigaData: [String: Any] = [
            "BoxItemTitle":self.datastore.boxItemTitle[i],
            "BoxItemImage":self.datastore.boxItemImage[i],
            "BoxItemComment":self.datastore.boxItemComment[i],
        ]
        }
    }
}
struct DecideScene_Previews: PreviewProvider {
    static var previews: some View {
        DecideScene()
    }
}
